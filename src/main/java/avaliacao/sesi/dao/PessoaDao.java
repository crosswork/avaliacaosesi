package avaliacao.sesi.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import avaliacao.sesi.entity.Pessoa;
import avaliacao.sesi.entity.enumeration.EstadoCivil;
import avaliacao.sesi.entity.enumeration.TipoPessoa;

public class PessoaDao {

	public static void criaTabelaAux() throws SQLException {
		Connection dbConnection = null;
		PreparedStatement pst = null;
		StringBuilder ddl = new StringBuilder("CREATE TABLE PESSOA ("
		        + "ID INTEGER NOT NULL, " + "ID_PAI INTEGER, "
		        + "NOME_RAZAO VARCHAR(20) NOT NULL, "
		        + "ESTADO VARCHAR(20) NOT NULL, "
		        + "MUNICIPIO VARCHAR(50) NOT NULL, "
		        + "CPF_CNPJ VARCHAR(50) NOT NULL, "
		        + "ESTADO_CIVIL VARCHAR(10), "
		        + "TIPO_PESSOA VARCHAR(2) NOT NULL, "
		        + "DATA_NASCIMENTO_FUNDACAO DATE NOT NULL, "
		        + "PRIMARY KEY (ID), "
		        + "FOREIGN KEY (ID_PAI) REFERENCES PESSOA (ID) ON DELETE CASCADE" +  ")"
		        		 );

		try {
			dbConnection = FabricaConexao.getConection();
			pst = dbConnection.prepareStatement(ddl.toString());

			System.out.println(ddl.toString());
			// execute the SQL stetement
			pst.executeUpdate();

			System.out.println("Tabela \"Pessoa\" criada com sucesso!");

		} catch (SQLException e) {

			e.printStackTrace();

		} finally {

			if (pst != null) {
				pst.close();
			}

			if (dbConnection != null) {
				dbConnection.close();
			}
		}
	}

	public Boolean  inserePessoa(Pessoa pessoa) throws SQLException {

		Connection con = null;
		PreparedStatement preparedStatement = null;

		StringBuilder sql = new StringBuilder(
		        "INSERT INTO PESSOA "
		                + "(ID_PAI, NOME_RAZAO, ESTADO,MUNICIPIO,CPF_CNPJ,ESTADO_CIVIL,TIPO_PESSOA,DATA_NASCIMENTO_FUNDACAO) VALUES "
		                + "(?,?,?,?,?,?,?,?) ");

		try {
			con = FabricaConexao.getConection();
			preparedStatement = con.prepareStatement(sql.toString());
			if ( pessoa.getIdPai() != null ) {
				preparedStatement.setLong(1,pessoa.getIdPai());				
			}else {
				preparedStatement.setNull(1, Types.INTEGER);
			}
			preparedStatement.setString(2, pessoa.getNomeRazaoSocial());
			preparedStatement.setString(3, pessoa.getUf());
			preparedStatement.setString(4, pessoa.getMunicipio());
			preparedStatement.setString(5, pessoa.getCnpjCpf());
			
			if ( pessoa.getEstadoCivil() != null ) {
				preparedStatement.setString(6, pessoa.getEstadoCivil().name());				
			}else {
				preparedStatement.setString(6,null);
			}
			
			preparedStatement.setString(7, pessoa.getTipoPessoa().name());
			preparedStatement.setTimestamp(8, new java.sql.Timestamp(pessoa
			        .getDataNascimentoFundacao().getTime()));
			// execute insert SQL stetement
			preparedStatement.executeUpdate();
		} catch (SQLException e) {

			e.printStackTrace();
			return false;

		} finally {

			if (preparedStatement != null) {
				preparedStatement.close();
			}

			if (con != null) {
				con.close();
			}

		}
		return true;

	}

	public List<Pessoa> listaPessoas() throws SQLException {

		Connection con = null;
		PreparedStatement preparedStatement = null;
		List<Pessoa> listaPessoas=new ArrayList<Pessoa>();

		String selectSQL = "SELECT * FROM PESSOA ";

		try {
			con = FabricaConexao.getConection();
			preparedStatement = con.prepareStatement(selectSQL);
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

			// execute select SQL stetement
			ResultSet rs = preparedStatement.executeQuery();

			while (rs.next()) {
				Pessoa pessoa = new Pessoa();
				pessoa.setId(rs.getLong("ID"));
				pessoa.setNomeRazaoSocial(rs.getString("NOME_RAZAO"));
				pessoa.setUf(rs.getString("ESTADO"));
				pessoa.setMunicipio(rs.getString("MUNICIPIO"));
				pessoa.setCnpjCpf(rs.getString("CPF_CNPJ"));
				
				if ( rs.getString("ESTADO_CIVIL") != null ) {
					pessoa.setEstadoCivil(rs.getString("ESTADO_CIVIL").equals(EstadoCivil.CASADO.name()) ? EstadoCivil.CASADO : EstadoCivil.SOLTEIRO);					
				}
				
				if ( rs.getString("TIPO_PESSOA") != null ) {
					pessoa.setTipoPessoa(rs.getString("TIPO_PESSOA").equals(TipoPessoa.PJ.name()) ? TipoPessoa.PJ : TipoPessoa.PF);					
				}
				
				pessoa.setDataNascimentoFundacao(rs.getDate("DATA_NASCIMENTO_FUNDACAO"));
				pessoa.setDataFormatada(sdf.format(pessoa.getDataNascimentoFundacao()));
                listaPessoas.add(pessoa);
			}

		} catch (SQLException e) {

			e.printStackTrace();

			return new ArrayList<Pessoa>();

		} finally {

			if (preparedStatement != null) {
				preparedStatement.close();
			}

			if (con != null) {
				con.close();
			}

		}

		return listaPessoas;

	}

	public  Pessoa recuperaPessoa(Long id) throws SQLException {

		Connection con = null;
		PreparedStatement preparedStatement = null;
        Pessoa pessoa = new Pessoa();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

		String selectSQL = "SELECT * FROM PESSOA WHERE ID = ? ";

		try {
			con = FabricaConexao.getConection();
			preparedStatement = con.prepareStatement(selectSQL);
            preparedStatement.setLong(1, id);

			// execute select SQL stetement
			ResultSet rs = preparedStatement.executeQuery();

			while (rs.next()) {
				pessoa.setId(rs.getLong("ID"));
				pessoa.setNomeRazaoSocial(rs.getString("NOME_RAZAO"));
				pessoa.setUf(rs.getString("ESTADO"));
				pessoa.setMunicipio(rs.getString("MUNICIPIO"));
				pessoa.setCnpjCpf(rs.getString("CPF_CNPJ"));
				if ( rs.getString("ESTADO_CIVIL") != null ) {
					pessoa.setEstadoCivil(rs.getString("ESTADO_CIVIL").equals(EstadoCivil.CASADO.name()) ? EstadoCivil.CASADO : EstadoCivil.SOLTEIRO);					
				}
				
				if ( rs.getString("TIPO_PESSOA") != null ) {
					pessoa.setTipoPessoa(rs.getString("TIPO_PESSOA").equals(TipoPessoa.PJ.name()) ? TipoPessoa.PJ : TipoPessoa.PF);					
				}
				pessoa.setDataNascimentoFundacao(rs.getDate("DATA_NASCIMENTO_FUNDACAO"));
				pessoa.setDataFormatada(sdf.format(pessoa.getDataNascimentoFundacao()));

			}

		} catch (SQLException e) {

			e.printStackTrace();
			return new Pessoa();

		} finally {

			if (preparedStatement != null) {
				preparedStatement.close();
			}

			if (con != null) {
				con.close();
			}

		}

		return pessoa;
	}

	public List<Pessoa> listaDependentesPessoa(Long idPai) throws SQLException {

        Connection con = null;
        PreparedStatement preparedStatement = null;
        List<Pessoa> listaPessoas=new ArrayList<Pessoa>();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        String selectSQL = "SELECT * FROM PESSOA WHERE ID_PAI = ?";

        try {
            con = FabricaConexao.getConection();
            preparedStatement = con.prepareStatement(selectSQL);
            preparedStatement.setLong(1, idPai);

            // execute select SQL stetement
            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next()) {
                Pessoa pessoa = new Pessoa();
                pessoa.setNomeRazaoSocial(rs.getString("NOME_RAZAO"));
                pessoa.setDataNascimentoFundacao(rs.getDate("DATA_NASCIMENTO_FUNDACAO"));
                pessoa.setDataFormatada(sdf.format(pessoa.getDataNascimentoFundacao()));
                listaPessoas.add(pessoa);
            }

        } catch (SQLException e) {

            e.printStackTrace();

            return new ArrayList<Pessoa>();

        } finally {

            if (preparedStatement != null) {
                preparedStatement.close();
            }

            if (con != null) {
                con.close();
            }

        }

        return listaPessoas;

    }

	public  Boolean removePessoaEDependentes(Long id) throws SQLException {

        Connection con = null;
        PreparedStatement preparedStatement = null;

        String deleteSQL = "DELETE FROM PESSOA WHERE ID = ? ";

        try {

            con = FabricaConexao.getConection();
            preparedStatement = con.prepareStatement(deleteSQL);
            preparedStatement.setLong(1, id);
            preparedStatement.executeUpdate();

        } catch (SQLException e) {

            e.printStackTrace();
            return false;

        } finally {

            if (preparedStatement != null) {
                preparedStatement.close();
            }

            if (con != null) {
                con.close();
            }

        }

        return true;
    }

	public Boolean  atualizaPessoa(Pessoa pessoa) throws SQLException {

        Connection con = null;
        PreparedStatement preparedStatement = null;

        StringBuilder sql = new StringBuilder(
            "UPDATE PESSOA SET "
                + " NOME_RAZAO = ?, ESTADO = ?, MUNICIPIO = ?, CPF_CNPJ =?, ESTADO_CIVIL =?,"
                + " TIPO_PESSOA = ?, DATA_NASCIMENTO_FUNDACAO =?, ID_PAI = ? WHERE ID = ? " );

        try {
            con = FabricaConexao.getConection();
            preparedStatement = con.prepareStatement(sql.toString());
            preparedStatement.setString(1, pessoa.getNomeRazaoSocial());
            preparedStatement.setString(2, pessoa.getUf());
            preparedStatement.setString(3, pessoa.getMunicipio());
            preparedStatement.setString(4, pessoa.getCnpjCpf());
            if ( pessoa.getEstadoCivil() != null ) {
				preparedStatement.setString(5, pessoa.getEstadoCivil().name());				
			}else {
				preparedStatement.setString(5,null);
			}
           
            
            preparedStatement.setString(6, pessoa.getTipoPessoa().name());
            preparedStatement.setTimestamp(7, new java.sql.Timestamp(pessoa.getDataNascimentoFundacao().getTime()));
            if ( pessoa.getIdPai() != null ) {
				preparedStatement.setLong(8,pessoa.getIdPai());				
			}else {
				preparedStatement.setNull(8, Types.INTEGER);
			}
            preparedStatement.setLong(9,pessoa.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {

            e.printStackTrace();
            return false;

        } finally {

            if (preparedStatement != null) {
                preparedStatement.close();
            }

            if (con != null) {
                con.close();
            }

        }
        return true;

    }
	
	
	public static void main(String[] args) {
		try {
			PessoaDao.criaTabelaAux();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


}
