package avaliacao.sesi.entity.enumeration;

public enum EstadoCivil {

	CASADO, SOLTEIRO;

}
