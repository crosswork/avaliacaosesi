package avaliacao.sesi.entity;

import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import avaliacao.sesi.entity.enumeration.EstadoCivil;
import avaliacao.sesi.entity.enumeration.TipoPessoa;


public class Pessoa {
	private Long id;
	private Long idPai;
	private String nomeRazaoSocial;
	private String municipio;
	private String uf;
	private Date dataNascimentoFundacao;
	private EstadoCivil estadoCivil;
	private TipoPessoa tipoPessoa;
	private String cnpjCpf;
	private List<Pessoa> dependentes;
	private String dataFormatada;
	
	public String getDataFormatada() {
		return dataFormatada;
	}

	public void setDataFormatada(String dataFormatada) {
		this.dataFormatada = dataFormatada;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<Pessoa> getDependentes() {
		return dependentes;
	}

	public void setDependentes(List<Pessoa> dependentes) {
		this.dependentes = dependentes;
	}

	public String getNomeRazaoSocial() {
		return nomeRazaoSocial;
	}

	public void setNomeRazaoSocial(String nomeRazaoSocial) {
		this.nomeRazaoSocial = nomeRazaoSocial;
	}

	public String getMunicipio() {
		return municipio;
	}

	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}

	public String getUf() {
		return uf;
	}

	public void setUf(String uf) {
		this.uf = uf;
	}

	public Date getDataNascimentoFundacao() {
		return dataNascimentoFundacao;
	}

	public void setDataNascimentoFundacao(Date dataNascimentoFundacao) {
		this.dataNascimentoFundacao = dataNascimentoFundacao;
	}

	public EstadoCivil getEstadoCivil() {
		return estadoCivil;
	}

	public void setEstadoCivil(EstadoCivil estadoCivil) {
		this.estadoCivil = estadoCivil;
	}

	public String getCnpjCpf() {
		return cnpjCpf;
	}

	public void setCnpjCpf(String cnpjCpf) {
		this.cnpjCpf = cnpjCpf;
	}

	public TipoPessoa getTipoPessoa() {
		return tipoPessoa;
	}

	public void setTipoPessoa(TipoPessoa tipoPessoa) {
		this.tipoPessoa = tipoPessoa;
	}

	public Long getIdPai() {
		return idPai;
	}

	public void setIdPai(Long idPai) {
		this.idPai = idPai;
	}

}
