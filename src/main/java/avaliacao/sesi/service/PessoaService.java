package avaliacao.sesi.service;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import avaliacao.sesi.dao.PessoaDao;
import avaliacao.sesi.entity.Envelope;
import avaliacao.sesi.entity.Pessoa;

public class PessoaService {

	public String listaPessoa(String caminhoXSL){

		try {
			List<Pessoa> pessoas = new ArrayList<Pessoa>();
			Envelope envelope = new Envelope();
			PessoaDao dao = new PessoaDao();
			pessoas = dao.listaPessoas();
			envelope.setListaPessoas(pessoas);
			String data = geraXMLFormatado(caminhoXSL+"ListaStylesheet.xsl",envelope);
			return data;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public String geraXMLFormatado(String caminhoXSL, Envelope envelope)
			throws JAXBException, TransformerFactoryConfigurationError, TransformerConfigurationException,
			TransformerException, UnsupportedEncodingException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		PrintStream ps = new PrintStream(baos, true, "UTF-8");
		StringWriter sw = new StringWriter();
		JAXBContext jaxbContext = JAXBContext.newInstance(Envelope.class);
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
		jaxbMarshaller.marshal(envelope, sw);


		Source source = new StreamSource(new StringReader( sw.toString() ) );
		Source xsl = new StreamSource(caminhoXSL);
		StreamResult result = new StreamResult(ps);
		TransformerFactory factory = TransformerFactory.newInstance();
		Transformer transformer = factory.newTransformer(xsl);
		transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		transformer.transform(source, result);
		String data = new String(baos.toByteArray(), StandardCharsets.UTF_8);
		return data;
	}

	public Boolean inserePessoa(Pessoa pessoa){
		try {
			PessoaDao dao = new PessoaDao();
			return dao.inserePessoa(pessoa);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public Boolean atualizaPessoa(Pessoa pessoa){
		try {
			PessoaDao dao = new PessoaDao();
			return dao.atualizaPessoa(pessoa);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	public Boolean deletaPessoa(Pessoa pessoa){
		try {
			PessoaDao dao = new PessoaDao();
			return dao.removePessoaEDependentes(pessoa.getId());
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public String manutencaoPessoa(String caminhoXSL,Long id){

		try {
			Pessoa pessoa = null;
			Envelope envelope = new Envelope();
			PessoaDao dao = new PessoaDao();
			if (id != null) {
				pessoa = dao.recuperaPessoa(id);
				List<Pessoa> listaDependentes = dao.listaDependentesPessoa(id);
				pessoa.setDependentes(listaDependentes);
			}else {
				pessoa = new Pessoa();
			}
			envelope.setPessoa(pessoa);
			String data = geraXMLFormatado(caminhoXSL+"EditStylesheet.xsl",envelope);
			return data;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

}
