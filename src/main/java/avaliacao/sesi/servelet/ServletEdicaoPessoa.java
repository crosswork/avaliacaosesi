package avaliacao.sesi.servelet;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import avaliacao.sesi.entity.Pessoa;
import avaliacao.sesi.entity.enumeration.EstadoCivil;
import avaliacao.sesi.entity.enumeration.TipoPessoa;
import avaliacao.sesi.service.PessoaService;

@WebServlet(urlPatterns="/salvapessoa")
public class ServletEdicaoPessoa extends HttpServlet{
	
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response)
	throws IOException{
		PessoaService service=new PessoaService();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Pessoa pessoa = new Pessoa();
		pessoa.setNomeRazaoSocial(request.getParameter("nomeRazaoSocial"));
		pessoa.setCnpjCpf(request.getParameter("cnpjCpf"));
		pessoa.setUf(request.getParameter("uf"));
		pessoa.setMunicipio(request.getParameter("municipio"));
		if (request.getParameter("idPai") != null && !request.getParameter("idPai").isEmpty()) {
			pessoa.setIdPai(new Long(request.getParameter("idPai")));			
		}
		
		if( request.getParameter("id") != null && !request.getParameter("id").isEmpty() ) {
			pessoa.setId(new Long(request.getParameter("id")));			
		}
		
		try {
			pessoa.setDataNascimentoFundacao(sdf.parse(request.getParameter("dataFormatada")));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if ( request.getParameter("estadoCivil") != null && !request.getParameter("estadoCivil").isEmpty() ) {
			
			pessoa.setEstadoCivil(request.getParameter("estadoCivil").equalsIgnoreCase("CASADO") ? EstadoCivil.CASADO : EstadoCivil.SOLTEIRO);
		}else {
			pessoa.setEstadoCivil(null);
		}
		
		if ( request.getParameter("tipoPessoa") != null ) {
			
			pessoa.setTipoPessoa(request.getParameter("tipoPessoa").equalsIgnoreCase("PJ") ? TipoPessoa.PJ : TipoPessoa.PF);
		}
		
		if( pessoa.getId() == null) {
			service.inserePessoa(pessoa);			
		}else {
			service.atualizaPessoa(pessoa);						
		}
			
		response.sendRedirect("/avaliacaojoin/listapessoa");
	}
	
	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response)
	throws IOException{
		PessoaService service=new PessoaService();
		String strDestino = request.getSession().getServletContext()
				.getRealPath("WEB-INF"+File.separator);
		
		Long id = request.getParameter("id") != null ? new Long(request.getParameter("id")) : null;
		response.getWriter().println(service.manutencaoPessoa(strDestino,id));	
	}
}


