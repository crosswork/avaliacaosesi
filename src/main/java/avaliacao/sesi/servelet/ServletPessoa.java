package avaliacao.sesi.servelet;

import java.io.File;
import java.io.IOException;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import avaliacao.sesi.service.PessoaService;

@WebServlet(urlPatterns="/listapessoa")
public class ServletPessoa extends HttpServlet{
	
	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response)
	throws IOException{
		PessoaService service=new PessoaService();
		String strDestino = request.getSession().getServletContext()
				.getRealPath("WEB-INF"+File.separator);
		response.getWriter().println(service.listaPessoa(strDestino));

		
	}
}


