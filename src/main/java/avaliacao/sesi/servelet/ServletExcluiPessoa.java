package avaliacao.sesi.servelet;

import java.io.IOException;
import java.text.SimpleDateFormat;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import avaliacao.sesi.entity.Pessoa;
import avaliacao.sesi.service.PessoaService;

@WebServlet(urlPatterns="/excluipessoa")
public class ServletExcluiPessoa extends HttpServlet{

	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException{
		PessoaService service=new PessoaService();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Pessoa pessoa = new Pessoa();
		pessoa.setId(new Long(request.getParameter("id")));			
		service.deletaPessoa(pessoa);						
		response.sendRedirect("/avaliacaojoin/listapessoa");
	}


}


