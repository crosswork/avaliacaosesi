package avaliacao.sesi.servelet;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import avaliacao.sesi.dao.FabricaConexao;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;

@WebServlet(urlPatterns="/rel")
public class ServletRelatorio extends HttpServlet{

	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException{
		geraRelatorio(request, response); 


	}

	public void geraRelatorio(HttpServletRequest request, HttpServletResponse response) throws IOException {
		JasperPrint jasperPrint;
		try {
			String reportFileName="pessoasreljasper.jrxml";
			String reportPath=request.getSession().getServletContext()
					.getRealPath("WEB-INF"+File.separator)+reportFileName;
			String targetFileName=reportFileName.replace(".jrxml", ".pdf");
			JasperReport jasperReport = JasperCompileManager.compileReport(reportPath); 
			jasperPrint = JasperFillManager.fillReport(jasperReport, null, FabricaConexao.getConection());
			ServletOutputStream outputstream = response.getOutputStream();
			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
			JasperExportManager.exportReportToPdfStream(jasperPrint, byteArrayOutputStream);
			response.setContentType("application/pdf");
			outputstream.write(byteArrayOutputStream.toByteArray());
			response.setHeader("Cache-Control", "max-age=0");
			response.setHeader("Content-Disposition", "attachment; filename=" + targetFileName); 
			outputstream.flush();
			outputstream.close(); 
		} catch (Exception e) {
			e.printStackTrace();
		}
	}




}


