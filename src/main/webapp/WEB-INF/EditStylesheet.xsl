<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="envelope/pessoa">
		<html>
			<body>
				<xsl:variable name="id" select="id" />
				<xsl:variable name="nomeRazaoSocial" select="nomeRazaoSocial" />
				<xsl:variable name="cnpjCpf" select="cnpjCpf" />
				<xsl:variable name="uf" select="uf" />
				<xsl:variable name="municipio" select="municipio" />
				<xsl:variable name="dataFormatada" select="dataFormatada" />
				<xsl:variable name="estadoCivil" select="estadoCivil" />
				<xsl:variable name="tipoPessoa" select="tipoPessoa" />
				<xsl:variable name="idPai" select="idPai" />

				<table>
					
					<form action="/avaliacaojoin/salvapessoa" method="post">
						<tr>
							<td>ID:</td>
							<td>
								<input type="text" name="id" readOnly="true" value="{$id}"></input>
							</td>
						</tr>
						<tr>
							<td>Nome/Raz�o Social:</td>
							<td>
								<input type="text" name="nomeRazaoSocial" value="{$nomeRazaoSocial}"></input>
							</td>
						</tr>
						<tr>
							<td>CPF/CNPJ:</td>
							<td>
								<input type="text" name="cnpjCpf" value="{$cnpjCpf}" />
							</td>
						</tr>
						<tr>
							<td>UF:</td>
							<td>
								<input type="text" name="uf" value="{$uf}" />
							</td>
						</tr>
						<tr>
							<td>Municipio:</td>
							<td>
								<input type="text" name="municipio" value="{$municipio}" />
							</td>
						</tr>
						<tr>
							<td>Data Nascimento/Funda��o:</td>
							<td>
								<input type="text" name="dataFormatada" value="{$dataFormatada}" />
							</td>
						</tr>
						<tr>
							<td>Estado Civ�l:</td>
							<td>
								<input type="text" name="estadoCivil" value="{$estadoCivil}" />
							</td>
						</tr>
						<tr>
							<td>Tipo Pessoa:</td>
							<td>
								<input type="text" name="tipoPessoa" value="{$tipoPessoa}" />
							</td>
						</tr>
						<tr>
							<td>ID Pai:</td>
							<td>
								<input type="text" name="idPai" value="{$idPai}" />
							</td>
						</tr>
						<tr>
							<td>
								<input type="submit" value="Salvar" />
							</td>
						</tr>
					</form>
					<form action="/avaliacaojoin/excluipessoa" method="post">
					<tr>
						<td>
							<input type="hidden" value="{id}" name="id" />
							<input type="submit" value="Excluir" />
						</td>
					</tr>
					</form>
					<table border="1">
						<tr bgcolor="#9acd32">

							<th>Nome/Raz�o Social</th>
							<th>Data Nascimento/Funda��o</th>
						</tr>
						<xsl:for-each select="dependentes">
							<tr>
								<td>
									<xsl:value-of select="nomeRazaoSocial" />
								</td>
								<td>
									<xsl:value-of select="dataFormatada" />
								</td>
							</tr>
						</xsl:for-each>
					</table>
				</table>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>