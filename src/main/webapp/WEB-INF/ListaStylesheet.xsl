<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<html>
			<body>
				<h2>Lista Pessoas</h2>
				<table border="1">
					<form action="/avaliacaojoin/salvapessoa" method="get">
						<tr>
							<td>
								<input type="submit" value="Novo" />
							</td>
						</tr>
					</form>
					<form action="/avaliacaojoin/rel" method="post">
						<tr>
							<td>
								<input type="submit" value="Imprimir" />
							</td>
						</tr>
					</form>
					<tr bgcolor="#9acd32">
						<th>ID</th>
						<th>Nome/Razão Social</th>
						<th>CPF/CNPJ</th>
						<th>Estado</th>
						<th>Município</th>
						<th>Data Nascimento/Fundação</th>
						<th>Tipo Pessoa</th>
						<th>Estado Civíl</th>
						<th>Editar</th>
						
					</tr>
					<xsl:for-each select="envelope/listaPessoas">
						<form action="/avaliacaojoin/salvapessoa" method="get">
							<tr>
								<td>
									<xsl:value-of select="id" />
								</td>
								<td>
									<xsl:value-of select="nomeRazaoSocial" />
								</td>
								<td>
									<xsl:value-of select="cnpjCpf" />
								</td>
								<td>
									<xsl:value-of select="uf" />
								</td>
								<td>
									<xsl:value-of select="municipio" />
								</td>
								<td>
									<xsl:value-of select="dataFormatada" />
								</td>
								<td>
									<xsl:value-of select="tipoPessoa" />
								</td>
								<td>
									<xsl:value-of select="estadoCivil" />
								</td>
								<td>
									<input type="hidden" value="{id}" name="id" />
									<input type="submit" value="Editar" />
								</td>
								
							</tr>
						</form>
					</xsl:for-each>
				</table>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>

